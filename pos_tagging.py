import nltk 
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize, sent_tokenize 
stop_words = set(stopwords.words('english')) 
sentence='yo'
tokenized_words = sent_tokenize(sentence) 
for i in tokenized_words: 
	wordsList = nltk.word_tokenize(i) 
	wordsList = [w for w in wordsList if not w in stop_words]  
	tagged = nltk.pos_tag(wordsList) 
	print(tagged) 
