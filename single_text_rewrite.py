from spacy.tokenizer import Tokenizer
from spacy.lang.en.examples import sentences
from synonym_generator import *


class TextRewrite:

    def __init__(self, sentence):
        self.sentence = sentence

    def work(self):
        """
        """
        rewrite_types = [u'NN', u'NNS', u'JJ', u'JJS']
        pos_tokenizer = nlp(unicode(self.sentence))
        words = []
        for token in pos_tokenizer:
            #print(token.pos_, token.text, token.tag_)
            if token.tag_ in rewrite_types:
                words.append(token.text)
        rewrited_sentence = self.sentence
        for word in words:
            word_syn = getSyn(word).get_words_list()
            rewrited_sentence = rewrited_sentence.replace(word, word_syn[0])
        return rewrited_sentence
 
