from nltk.corpus import wordnet
import spacy
import urllib.request
import json

spacy_word = spacy.load('en')

class getSyn:

    def get_datamuse_syn_list(self):
        url = "https://api.datamuse.com/words?ml=" + self.word
        response = urllib.request.urlopen(url)
        data = response.read().decode("utf-8")
        json_data = json.loads(data)
        word_list = []
        for x in json_data:
            word_list.append(x['word'])
        return word_list

    def __init__(self, word):
        self.word = word
        self.score = 0.5
        self.choice = []


    def get_words_list(self):
        words_list = self.get_datamuse_syn_list()
        for syn_word in words_list:
            use_nltk = True
            try:
                nltk_raw_word = wordnet.synsets(self.word)[0]
                nltk_syn_word = wordnet.synsets(syn_word)[0]
            except:
                use_nltk = False
            
            spacy_raw_word = spacy_word(str(self.word.lower()))
            spacy_syn_word = spacy_word(str(syn_word.lower()))

            
            spacy_score = spacy_raw_word.similarity(spacy_syn_word)
            
            if (use_nltk == True):
                nltk_score = nltk_syn_word.wup_similarity(nltk_raw_word)
                if (nltk_score == None):
                    nltk_score = 0
                score = (nltk_score+spacy_score)/2
            else:
                score = spacy_score

            if (score > self.score):
                self.choice.append(syn_word)
        return self.choice
